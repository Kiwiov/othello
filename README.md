#Othello

**Chosing gamemode**

When the program is started the player will be questioned if they want to play against an AI, if input is Yes (Y/y) the player will face an AI opponent, if input is No (N/n) the player will control both players.

**Making a move**

The player makes a move by entering two numbers without spaces, like 32. The first number is the X value while the second number is the Y value. Entering 32 will try to place a token at the position (3,2), while entering 81 will try to place a token at (8,1). A move will only be accepted if its is legal. If a illegal move is entered the player will be told so and will get to try again.