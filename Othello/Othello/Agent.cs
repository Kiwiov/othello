﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Othello
{
    class Agent
    {
        /// <summary>
        /// Used by game to fetch chosen move.
        /// </summary>
        public Option GetMove(Board gameState)
        {
            // Very basic "AI" should be replaced by actual AI code!
            {
                List<Option> moves = gameState.GetOptions(Token.Player2);
                moves.Sort();

                Console.WriteLine("Chosen Play: x: " + (moves.Last().Point.Col + 1) + ", y: " + (moves.Last().Point.Row + 1));
                Console.ReadKey();

                return moves.Last();
            }
        }
    }
}