﻿///----------------------------------------
/// Copyright (C) Hugo Hansen & Fredrik Sy
///----------------------------------------

using System;

namespace Othello
{
    class Program
    {
        static void Main(string[] args)
        {
            string illegal = "Illegal Move! Try Again!";

            // Main loop.
            while (true)
            {
                Board board = new Board(ConsoleColor.Cyan, ConsoleColor.Red, ConsoleColor.White, "P1", "P2");
                Agent agent = new Agent();

                bool ai = false;
                bool p1Turn = true;

                // Menu loop.
                String option = "";
                while (option == "")
                {
                    Console.Clear();
                    Console.WriteLine("Play Vs AI? (Y/N)");
                    option = Console.ReadLine();

                    if (option == "N" || option == "n")
                    {
                        ai = false;
                    }
                    else if (option == "Y" || option == "y")
                    {
                        ai = true;
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("Try Again");
                        Console.ReadKey();
                        option = "";
                    }
                }

                // Game loop.
                while (!board.GameOver)
                {
                    board.RenderBoard();

                    // Player 1 logic.
                    if (p1Turn)
                    {
                        if (board.GetOptions(Token.Player1).Count != 0)
                        {
                            Console.WriteLine("Player1s Turn!");
                            string input = Console.ReadLine();
                            if (input.Length >= 2 && int.TryParse(input, out int temp))
                            {
                                int.TryParse(input[0].ToString(), out int x);
                                int.TryParse(input[1].ToString(), out int y);

                                // Legal move?
                                if (board.TryMove(x, y, Token.Player1))
                                {
                                    p1Turn = !p1Turn;
                                }
                                else
                                {
                                    Console.Clear();
                                    Console.WriteLine(illegal);
                                    Console.ReadKey();
                                }
                            }
                            else
                            {
                                Console.Clear();
                                Console.WriteLine(illegal);
                                Console.ReadKey();
                            }
                        }
                        else
                        {
                            p1Turn = !p1Turn;
                        }
                    }
                    // Player 2 logic.
                    else
                    {
                        if (board.GetOptions(Token.Player2).Count != 0)
                        {
                            Console.WriteLine("Player2s Turn!");

                            // AI logic.
                            if (ai)
                            {
                                Option move = agent.GetMove(board);
                                board.TryMove(move.Point.Col + 1, move.Point.Row + 1, Token.Player2);
                                p1Turn = !p1Turn;
                            }
                            // Player logic.
                            else
                            {
                                string input = Console.ReadLine();
                                if (input.Length >= 2 && int.TryParse(input, out int temp))
                                {
                                    int.TryParse(input[0].ToString(), out int x);
                                    int.TryParse(input[1].ToString(), out int y);

                                    // Legal move?
                                    if (board.TryMove(x, y, Token.Player2))
                                    {
                                        p1Turn = !p1Turn;
                                    }
                                    else
                                    {
                                        Console.Clear();
                                        Console.WriteLine(illegal);
                                        Console.ReadKey();
                                    }
                                }
                                else
                                {
                                    Console.Clear();
                                    Console.WriteLine(illegal);
                                    Console.ReadKey();
                                }
                            }
                        }
                        else
                        {
                            p1Turn = !p1Turn;
                        }
                    }

                    //PostRound Time.
                    board.PostRound();

                    //End Screen!
                    if (board.GameOver)
                    {
                        board.RenderBoard();
                        Console.WriteLine("Game Over!");
                        if (board.ScoreP1 == board.ScoreP2)
                            Console.WriteLine("Its a Draw!");
                        else
                            Console.WriteLine(board.ScoreP1 > board.ScoreP2 ? "Player 1 Wins!" : "Player 2 Wins!");
                        Console.ReadKey();
                    }
                }
            }
        }
    }
}
