﻿///----------------------------------------
/// Copyright (C) Hugo Hansen & Fredrik Sy
///----------------------------------------

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Othello
{
    enum Token
    {
        Player1,
        Player2,
        Empty
    }

    /// <summary>
    /// Does not use x, y! Uses Row, Col!
    /// </summary>
    class Point
    {
        private int row;
        private int col;

        public Point(int row, int col)
        {
            this.row = row;
            this.col = col;
        }

        public int Col
        {
            get { return col; }
            set { col = value; }
        }

        public int Row
        {
            get { return row; }
            set { row = value; }
        }

    }

    /// <summary>
    /// Represents a possible move, contains the Point and Worth. (Can be compared by worth!).
    /// </summary>
    class Option : IComparable<Option>
    {
        private Point point;
        private int worth;

        public Option(Point point, int worth)
        {
            this.point = point;
            this.worth = worth;
        }

        public int Worth
        {
            get { return worth; }
            set { worth = value; }
        }

        public Point Point
        {
            get { return point; }
            set { point = value; }
        }

        public int CompareTo(Option other)
        {
            if (Worth > other.Worth)
                return 1;
            else if (Worth < other.Worth)
                return -1;
            else
                return 0;
        }
    }

    /// <summary>
    /// GameBoard / GameState (Use Clone() to get deep copy!).
    /// </summary>
    [Serializable]
    class Board
    {
        private Token[,] tokens; //[Row, Col]

        private ConsoleColor colorP1;
        private ConsoleColor colorP2;
        private ConsoleColor colorInfo;

        private int scoreP1;
        private int scoreP2;

        private string nameP1;
        private string nameP2;

        private bool gameOver;

        /// <summary>
        /// Represents if the game is over.
        /// </summary>
        public bool GameOver
        {
            get { return gameOver; }
            set { gameOver = value; }
        }

        /// <summary>
        /// Represents the score of player 2.
        /// </summary>
        public int ScoreP2
        {
            get { return scoreP2; }
            private set { scoreP2 = value; }
        }

        /// <summary>
        /// Represents the score of player 1.
        /// </summary>
        public int ScoreP1
        {
            get { return scoreP1; }
            private set { scoreP1 = value; }
        }

        /// <summary>
        /// Represents all positions of the board (Row, Col).
        /// </summary>
        public Token[,] Tokens
        {
            get { return tokens; }
            private set { tokens = value; }
        }

        /// <summary>
        /// Board constructor, sets needed variables and runs MakeBoard().
        /// </summary>
        public Board(ConsoleColor colorP1, ConsoleColor colorP2, ConsoleColor colorInfo, string nameP1, string nameP2)
        {
            this.colorP1 = colorP1;
            this.colorP2 = colorP2;
            this.colorInfo = colorInfo;

            this.nameP1 = nameP1;
            this.nameP2 = nameP2;
            
            MakeBoard();
        }

        /// <summary>
        /// Clears the board and resets board/variables to starting state/values.
        /// </summary>
        public void MakeBoard()
        {
            //Create board.
            tokens = new Token[8, 8];

            //Set tokens.
            for (int r = 0; r < tokens.GetLength(0); r++)
            {
                for (int c = 0; c < tokens.GetLength(1); c++)
                {
                    tokens[r, c] = Token.Empty;
                }
            }
            tokens[3, 3] = Token.Player1;
            tokens[3, 4] = Token.Player2;
            tokens[4, 4] = Token.Player1;
            tokens[4, 3] = Token.Player2;

            scoreP1 = 0;
            scoreP2 = 0;

            gameOver = false;
        }

        /// <summary>
        /// "Renders" the game in the console window.
        /// </summary>
        public void RenderBoard()
        {
            int width = tokens.GetLength(0);
            int height = tokens.GetLength(1);

            string sToken = " O";
            string sEmpty = "  ";

            Console.Clear();

            //Player 1 Score
            Console.Write(nameP1 + ": ");
            Console.ForegroundColor = colorP1;
            Console.Write(scoreP1);
            Console.ForegroundColor = colorInfo;

            Console.Write(" ---- ");

            //Player 2 Score
            Console.Write(nameP2 + ": ");
            Console.ForegroundColor = colorP2;
            Console.Write(scoreP2);
            Console.ForegroundColor = colorInfo;

            Console.WriteLine();
            Console.WriteLine();

            //Plot GameBoard
            for (int r = 0; r <= width; r++)
            {
                for (int c = 0; c <= height; c++)
                {
                    if (r == 0)
                        Console.Write(" " + c);
                    else if (c == 0)
                        Console.Write(" " + r);
                    else
                    {
                        if (tokens[r - 1, c - 1] == Token.Player1)
                        {
                            Console.ForegroundColor = colorP1;
                            Console.Write(sToken);
                            Console.ForegroundColor = colorInfo;
                        }
                        else if (tokens[r - 1, c - 1] == Token.Player2)
                        {
                            Console.ForegroundColor = colorP2;
                            Console.Write(sToken);
                            Console.ForegroundColor = colorInfo;
                        }
                        else
                            Console.Write(sEmpty);
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Returns the legal moves of the player provided.
        /// </summary>
        public List<Option> GetOptions(Token playerToken)
        {
            List<Option> options = new List<Option>();
            for (int r = 0; r < 8; r++)
            {
                for (int c = 0; c < 8; c++)
                {
                    int worth = MoveWorth(r, c, playerToken);
                    if (worth != -1)
                        options.Add(new Option(new Point(r, c), worth));
                }
            }

            return options;
        }

        /// <summary>
        /// Returns move worth, returns -1 if illegal.
        /// </summary>
        public int MoveWorth(int row, int col, Token playedToken)
        {
            List<Point> endPoints = new List<Point>();
            if (LegalMove(row, col, playedToken, ref endPoints))
            {
                int deltaRow, deltaCol, worth = 0;
                foreach (Point point in endPoints)
                {
                    deltaRow = Math.Abs(point.Row - row);
                    deltaCol = Math.Abs(point.Col - col);

                    worth += deltaRow >= deltaCol ? deltaRow - 1 : deltaCol - 1;
                }
                worth++;
                return worth;
            }

            return -1;
        }

        /// <summary>
        /// Tries to make move at given position for given player, returns false if illegal. (Uses x,y! Think of x as col + 1 and y as row + 1).
        /// </summary>
        public bool TryMove(int x, int y, Token playedToken)
        {
            int row = y - 1, col = x - 1;
            List<Point> endPoints = new List<Point>();

            //Move legal? then makemove.
            if (LegalMove(row, col, playedToken, ref endPoints))
            {
                foreach (Point ePoint in endPoints)
                {
                    int normalRow = 0;
                    int normalCol = 0;
                    int currentRow = row;
                    int currentCol = col;

                    if (ePoint.Row > row)
                        normalRow = 1;
                    else if (ePoint.Row < row)
                        normalRow = -1;

                    if (ePoint.Col > col)
                        normalCol = 1;
                    else if (ePoint.Col < col)
                        normalCol = -1;

                    while (currentRow != ePoint.Row || currentCol != ePoint.Col)
                    {
                        Tokens[currentRow, currentCol] = playedToken;
                        currentRow += normalRow;
                        currentCol += normalCol;
                    }

                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Sets score and checks if game is over. Should call after a move has been made!
        /// </summary>
        public void PostRound()
        {
            bool terminal = true;
            ScoreP1 = 0;
            ScoreP2 = 0;

            for (int r = 0; r < tokens.GetLength(0); r++)
            {
                for (int c = 0; c < tokens.GetLength(1); c++)
                {
                    if (tokens[r,c] == Token.Empty)
                        terminal = false;
                    else if (tokens[r, c] == Token.Player1)
                        ScoreP1++;
                    else
                        ScoreP2++;
                }
            }
            gameOver = terminal;
        }

        /// <summary>
        /// Used to get a deep copy.
        /// </summary>
        public Board Clone()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(stream, this);
                stream.Position = 0;
                return (Board)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Checks if move is legal.
        /// </summary>
        private bool LegalMove(int row, int col, Token playedToken, ref List<Point> endPoints)
        {
            bool legal = false;

            if (playedToken != Token.Empty)
            {
                Token evil = playedToken == Token.Player1 ? Token.Player2 : Token.Player1;

                if (row >= 0 && row < tokens.GetLength(0) && col >= 0 && col < tokens.GetLength(1))
                {
                    if (tokens[row,col] == Token.Empty)
                    {
                        //+ Tests
                        {
                            //West
                            if (col != 0 && tokens[row, col - 1] == evil)
                            {
                                for (int c = col - 1; c >= 0; c--)
                                {
                                    if (tokens[row, c] == Token.Empty)
                                        break;

                                    if (tokens[row, c] == playedToken)
                                    {
                                        endPoints.Add(new Point(row, c));
                                        legal = true;
                                        break;
                                    }
                                }
                            }


                            //East
                            if (col != tokens.GetLength(1) - 1 && tokens[row, col + 1] == evil)
                            {
                                for (int c = col + 1; c < tokens.GetLength(1); c++)
                                {
                                    if (tokens[row, c] == Token.Empty)
                                        break;

                                    if (tokens[row, c] == playedToken)
                                    {
                                        endPoints.Add(new Point(row, c));
                                        legal = true;
                                        break;
                                    }
                                }
                            }

                            //North
                            if (row != 0 && tokens[row - 1, col] == evil)
                            {
                                for (int r = row - 1; r >= 0; r--)
                                {
                                    if (tokens[r, col] == Token.Empty)
                                        break;

                                    if (tokens[r, col] == playedToken)
                                    {
                                        endPoints.Add(new Point(r, col));
                                        legal = true;
                                        break;
                                    }
                                }
                            }

                            //South
                            if (row != tokens.GetLength(0) - 1 && tokens[row + 1, col] == evil)
                            {
                                for (int r = row + 1; r < tokens.GetLength(0); r++)
                                {
                                    if (tokens[r, col] == Token.Empty)
                                        break;

                                    if (tokens[r, col] == playedToken)
                                    {
                                        endPoints.Add(new Point(r, col));
                                        legal = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //x Tests
                        {
                            //NorthWest
                            if (col != 0 && row != 0 && tokens[row - 1, col - 1] == evil)
                            {
                                if (col <= row)
                                {
                                    int r = row - 1;
                                    for (int c = col - 1; c >= 0; c--, r--)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int c = col - 1;
                                    for (int r = row - 1; r >= 0; c--, r--)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            //NorthEast
                            if (col != tokens.GetLength(1) - 1 && row != 0 && tokens[row - 1, col + 1] == evil)
                            {
                                if (tokens.GetLength(1) - 1 - col <= row)
                                {
                                    int r = row - 1;
                                    for (int c = col + 1; c < tokens.GetLength(1); c++, r--)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int c = col + 1;
                                    for (int r = row - 1; r >= 0; c++, r--)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            //SouthWest
                            if (row != tokens.GetLength(0) - 1 && col != 0 && tokens[row + 1, col - 1] == evil)
                            {
                                if (tokens.GetLength(0) - 1 - row <= col)
                                {
                                    int c = col - 1;
                                    for (int r = row + 1; r < tokens.GetLength(0); c--, r++)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int r = row + 1;
                                    for (int c = col - 1; c >= 0; c--, r++)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                            }

                            //SouthEast
                            if (row != tokens.GetLength(0) - 1 && col != tokens.GetLength(1) - 1 && tokens[row + 1, col + 1] == evil)
                            {
                                if (col >= row)
                                {
                                    int r = row + 1;
                                    for (int c = col + 1; c < tokens.GetLength(1); c++, r++)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    int c = col + 1;
                                    for (int r = row + 1; r < tokens.GetLength(0); c++, r++)
                                    {
                                        if (tokens[r, c] == Token.Empty)
                                            break;

                                        if (tokens[r, c] == playedToken)
                                        {
                                            endPoints.Add(new Point(r, c));
                                            legal = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return legal;
        }
    }
}
